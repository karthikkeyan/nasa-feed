//
//  ViewController.swift
//  Nasa
//
//  Created by Karthikkeyan Bala Sundaram on 4/6/19.
//

import UIKit
import ViewModelKit
import Reachability

final class FeedViewController: UIViewController {

    enum Segue: String {
        case feedToMediaDetails
    }

    private let searchFieldCollapsedWidth: CGFloat = 32

    @IBOutlet private weak var searchIconView: UIImageView!
    @IBOutlet private weak var searchButton: UIButton!
    @IBOutlet private weak var searchTextField: UITextField!
    @IBOutlet private weak var searchView: UIView!
    @IBOutlet private weak var searchViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var titleImageView: UIImageView!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var emptyView: UIImageView!

    private var footerView: UIView?

    private lazy var viewModel = FeedViewModel()
    private let reachability = Reachability()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.contentInsetAdjustmentBehavior = .never
        footerView = tableView.tableFooterView
        tableView.isHidden = true

        setupNetworkConnectionMonitoring()
        loadFeed(query: "")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIWindow.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIWindow.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide(notification:)), name: UIWindow.keyboardDidHideNotification, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self, name: UIWindow.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIWindow.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIWindow.keyboardDidHideNotification, object: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier, let type = Segue(rawValue: identifier) else {
            return
        }

        switch type {
        case .feedToMediaDetails:
            if let indexPath = sender as? IndexPath {
                let item = viewModel.media[indexPath.row]
                (segue.destination as? MediaDetailsViewController)?.feedItem = item
            }
        }
    }

    // MARK: - Event Handlers

    @IBAction func unwindFromFeedDetails(segue: UIStoryboardSegue) { }

    @IBAction private func didSelectShowSearch() {
        let isCollapsed = searchFieldCollapsedWidth == searchViewWidthConstraint.constant
        guard isCollapsed else { return }

        showSearch(true)
        searchTextField.becomeFirstResponder()
    }

    // MARK: - Private Methods

    private func setupNetworkConnectionMonitoring() {
        reachability?.whenReachable = { [weak self] _ in
            guard let strongSelf = self, strongSelf.viewModel.media.isEmpty else { return }
            strongSelf.loadFeed(query: strongSelf.viewModel.query)
        }

        do {
            try reachability?.startNotifier()
        } catch {
            Log.debug(message: error)
        }
    }

    private func showSearch(_ show: Bool, animated: Bool = true) {
        if show {
            let searchViewHorizontalSpacing: CGFloat = 32
            searchViewWidthConstraint.constant = view.bounds.width - searchViewHorizontalSpacing
        } else {
            searchViewWidthConstraint.constant = searchFieldCollapsedWidth
        }

        let animations = { () -> Void in
            if show {
                self.searchIconView.tintColor = .lightGray
                self.searchView.backgroundColor = Color.lightBackground.color
                self.searchButton.isEnabled = false
                self.titleImageView.alpha = 0.0
            } else {
                self.searchIconView.tintColor = Color.text.color
                self.searchView.backgroundColor = .clear
                self.searchButton.isEnabled = true
                self.titleImageView.alpha = 1.0
            }

            self.view.layoutIfNeeded()
        }

        if animated {
            UIView.animate(withDuration: 0.3, animations: animations)
        } else {
            animations()
        }
    }

    private func cancelSearchEdit() {
        var text = searchTextField.text ?? ""
        if text != viewModel.query {
            searchTextField.text = viewModel.query
        }

        text = searchTextField.text ?? ""
        if text.isEmpty {
            showSearch(false)
        }
    }

    private func loadFeed(query: String) {
        let clearPrevious = viewModel.query != query
        if clearPrevious {
            tableView.isHidden = true
            tableView.setContentOffset(CGPoint(x: 0, y: 1), animated: false)
        }

        let requireSpinner = viewModel.media.isEmpty || clearPrevious
        if requireSpinner {
            CircularSpinnerView.show(in: view)
        }

        emptyView.isHidden = true

        viewModel.loadNexPage(query: query) { [weak self] result in
            DispatchQueue.main.async {
                guard let strongSelf = self else { return }

                switch result {
                case .success(let page):
                    if requireSpinner {
                        strongSelf.tableView.reloadData()
                    } else {
                        if let unwrappedPage = page {
                            strongSelf.tableView.beginUpdates()
                            strongSelf.tableView.insertRows(at: unwrappedPage.indexPaths, with: .automatic)
                            strongSelf.tableView.endUpdates()
                        }
                    }
                    strongSelf.tableView.tableFooterView = strongSelf.viewModel.canloadMore ? strongSelf.footerView : nil
                case .failure(let error):
                    let controller = UIAlertController(title: "Feed".localized(), message: error.localizedDescription, preferredStyle: .alert)
                    controller.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil))
                    strongSelf.present(controller, animated: true, completion: nil)
                }

                strongSelf.emptyView.isHidden = !strongSelf.viewModel.media.isEmpty

                if requireSpinner {
                    CircularSpinnerView.hide(from: strongSelf.view)
                }
            }
        }
    }

}

// MARK: - UITableViewDataSource

extension FeedViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.isHidden = viewModel.media.isEmpty
        return viewModel.media.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedTableViewCell.cellIdentifier, for: indexPath)
        (cell as? FeedTableViewCell)?.update(using: viewModel.media[indexPath.row], viewModel: viewModel, indexPath: indexPath)
        return cell
    }

}

// MARK: - UITableViewDelegate

extension FeedViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        cancelSearchEdit()
        performSegue(withIdentifier: Segue.feedToMediaDetails.rawValue, sender: indexPath)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.row == viewModel.media.count - 1 else { return }
        loadFeed(query: viewModel.query)
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }

}

// MARK: - UITextFieldDelegate

extension FeedViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()

        if let text = textField.text, !text.isEmpty {
            loadFeed(query: text)
        } else {
            viewModel.clearData()
            tableView.reloadData()
            loadFeed(query: "")
            showSearch(false)
        }
        return false
    }

}

// MARK: - Notifications

extension FeedViewController {

    @objc private func keyboardWillShow(notification: Notification) {
        guard let property = notification.keyboardProperty else { return }

        updatePatientListContentInset(forKeyboardHeight: property.endRect.height)
    }

    @objc private func keyboardWillHide(notification: Notification) {
        updatePatientListContentInset(forKeyboardHeight: 0)
    }

    @objc private func keyboardDidHide(notification: Notification) {
        cancelSearchEdit()
    }

    private func updatePatientListContentInset(forKeyboardHeight height: CGFloat) {
        tableView.contentInset.bottom = height
        tableView.scrollIndicatorInsets.bottom = height
    }

}

// MARK: - FeedTableViewCell

fileprivate extension FeedTableViewCell {

    static let mediaPlaceholder = UIImage(named: "imgPlaceholder")

    func update(using item: FeedItem, viewModel: FeedViewModel, indexPath: IndexPath) {
        self.indexPath = indexPath
        mediaTitleLabel.text = item.data.title
        if let image = AssetDownloader.cachedImage(forItem: item) {
            mediaImageView.image = image
        } else {
            mediaImageView.image = FeedTableViewCell.mediaPlaceholder

            AssetDownloader.downloadMedia(of: item) { [weak self] image in
                guard self?.indexPath == indexPath, let unwrappedImage = image else { return }

                self?.mediaImageView.image = unwrappedImage
            }
        }
    }

}
