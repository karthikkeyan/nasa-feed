//
//  KeywordCollectionViewCell.swift
//  Nasa
//
//  Created by Karthikkeyan Bala Sundaram on 4/7/19.
//

import UIKit

class KeywordCollectionViewCell: UICollectionViewCell, CellIdentifierProvider {

    static var cellIdentifier: String = "KeywordCollectionViewCellID"

    @IBOutlet weak var titleLabel: UILabel!

    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var frame = layoutAttributes.frame
        frame.size.height = 32
        frame.size.width = size.width
        layoutAttributes.frame = frame
        return layoutAttributes
    }

}
