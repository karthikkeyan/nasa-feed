//
//  MediaDetailsViewController.swift
//  Nasa
//
//  Created by Karthikkeyan Bala Sundaram on 4/7/19.
//

import UIKit
import ViewModelKit

final class MediaDetailsViewController: UIViewController {

    var feedItem: FeedItem?

    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var mediaImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var collectionViewHeightConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        scrollView.contentInsetAdjustmentBehavior = .never
        setupCollectionViewLayout()
        updateLabels()
        collectionView.reloadData()

        downloadImage()
    }

    // MARK: - Private Method

    private func setupCollectionViewLayout() {
        let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        flowLayout?.estimatedItemSize = CGSize(width: 50, height: 32)
        flowLayout?.itemSize = UICollectionViewFlowLayout.automaticSize
        flowLayout?.minimumLineSpacing = 8
        flowLayout?.minimumInteritemSpacing = 8

        collectionViewHeightConstraint.constant = 48
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 16, right: 8)
    }

    private func updateLabels() {
        titleLabel.text = feedItem?.data.title
        if let date = feedItem?.data.dateCreated {
            dateLabel.text = DateFormatter.standardFormat.string(from: date)
        } else {
            dateLabel.text = "Date Unavilable".localized()
        }

        if let description = feedItem?.data.description, !description.isEmpty {
            descriptionLabel.text = description
        } else {
            descriptionLabel.text = "Media description not available".localized()
        }
    }

    private func downloadImage() {
        guard let item = feedItem else { return }

        if let image = AssetDownloader.cachedImage(forItem: item) {
            mediaImageView.image = image
        } else {
            AssetDownloader.downloadMedia(of: item) { [weak self] image in
                guard let unwrappedImage = image else { return }

                self?.mediaImageView.image = unwrappedImage
            }
        }
    }

}

// MARK: - UICollectionViewDataSource

extension MediaDetailsViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return feedItem?.data.keywords?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: KeywordCollectionViewCell.cellIdentifier, for: indexPath)
        (cell as? KeywordCollectionViewCell)?.titleLabel.text = feedItem?.data.keywords?[indexPath.section]
        return cell
    }

}

// MARK: - UICollectionViewDelegate

extension MediaDetailsViewController: UICollectionViewDelegate { }

// MARK: - UIScrollViewDelegate

extension MediaDetailsViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard scrollView.contentOffset.y <= 0 else {
            mediaImageView.transform = .identity
            return
        }

        let minimumScale: CGFloat = 1
        // The number 1.6 for maximum scale is obtained based on few trail and error
        let maximumScale: CGFloat = 1.6
        let maximumScrollPullDownOffset: CGFloat = -100
        let scale = straightLineY(x: scrollView.contentOffset.y, x1: 0, x2: maximumScrollPullDownOffset, y1: minimumScale, y2: maximumScale)
        mediaImageView.transform = CGAffineTransform(scaleX: scale, y: scale)
    }

    private func straightLineY(x: CGFloat, x1: CGFloat, x2: CGFloat, y1: CGFloat, y2: CGFloat) -> CGFloat {
        // y = mx + c
        // c = y - mx       where y = any y, x = any x
        let m = (y2 - y1) / (x2 - x1)
        let c = y1 - (m * x1)
        return (m * x) + c
    }

}
