//
//  FeedTableViewCell.swift
//  Nasa
//
//  Created by Karthikkeyan Bala Sundaram on 4/7/19.
//

import UIKit

class FeedTableViewCell: UITableViewCell, CellIdentifierProvider {

    static let cellIdentifier: String = "FeedTableViewCellID"

    private let touchAnimationDuration: TimeInterval = 0.15

    @IBOutlet weak var mediaContainerView: UIView!
    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var mediaShadowImageView: UIImageView!
    @IBOutlet weak var mediaTitleLabel: UILabel!

    var indexPath: IndexPath?

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)

        UIView.animate(withDuration: touchAnimationDuration) {
            let transitionScale: CGFloat = 0.95
            let transform = CATransform3DScale(CATransform3DIdentity, transitionScale, transitionScale, transitionScale)
            self.mediaShadowImageView.layer.transform = transform
            self.mediaContainerView.layer.transform = transform
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)

        resetTouchTransition()
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)

        resetTouchTransition()
    }

    // MARK: - Private Methods

    private func resetTouchTransition() {
        UIView.animate(withDuration: touchAnimationDuration) {
            self.mediaShadowImageView.layer.transform = CATransform3DIdentity
            self.mediaContainerView.layer.transform = CATransform3DIdentity
        }
    }

}
