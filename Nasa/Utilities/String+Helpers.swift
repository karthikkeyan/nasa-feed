//
//  String+Helpers.swift
//  Nasa
//
//  Created by Karthikkeyan Bala Sundaram on 4/7/19.
//

import Foundation

extension String {

    func localized(comment: String = "") -> String {
        return NSLocalizedString(self, comment: comment)
    }

}
