//
//  Notification+Keyboard.swift
//  Compass
//
//  Created by Karthikkeyan Bala Sundaram on 1/31/18.
//  Copyright © 2019 Karthik. All rights reserved.
//

import UIKit

extension Notification {

    struct KeyboardProperty {
        var animationDuration: TimeInterval = 0
        var animationOptions: UIView.AnimationOptions = .curveLinear
        var endRect: CGRect = .zero
        var beginRect: CGRect = .zero
    }

    var keyboardProperty: KeyboardProperty? {
        guard let isKeyboardNotification = (userInfo?[UIResponder.keyboardIsLocalUserInfoKey] as? NSNumber)?.boolValue, isKeyboardNotification else {
            return nil
        }

        var properties = KeyboardProperty()
        if let duration = keyboardAnimationDuration {
            properties.animationDuration = duration
        }

        if let options = keyboardAnimationOptions {
            properties.animationOptions = options
        }

        if let rect = keyboardBeginRect {
            properties.beginRect = rect
        }

        if let rect = keyboardEndRect {
            properties.endRect = rect
        }

        return properties
    }

    private var keyboardAnimationDuration: TimeInterval? {
        guard let interval = (userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue else {
            return nil
        }

        return TimeInterval(interval)
    }

    private var keyboardAnimationOptions: UIView.AnimationOptions? {
        guard let curveObject = userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber else {
            return nil
        }

        return UIView.AnimationOptions(rawValue: UInt(curveObject.uintValue))
    }

    private var keyboardEndRect: CGRect? {
        return (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
    }

    private var keyboardBeginRect: CGRect? {
        return (userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
    }

}
