//
//  NasaUIMisc.swift
//  Nasa
//
//  Created by Karthikkeyan Bala Sundaram on 4/7/19.
//

import UIKit

protocol CellIdentifierProvider {
    static var cellIdentifier: String { get }
}

enum Color: String {
    case lightBackground = "colLightBackground"
    case text = "colText"

    var color: UIColor {
        guard let unwrappedColor = UIColor(named: rawValue) else {
            fatalError("Unable to create name color \(rawValue)")
        }
        return unwrappedColor
    }
}
