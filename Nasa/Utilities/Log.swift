//
//  Log.swift
//  Nasa
//
//  Created by Karthikkeyan Bala Sundaram on 4/8/19.
//

import Foundation

struct Log {

    static func debug(message: Any) {
#if DEBUG
        print(message)
#endif
    }

}
