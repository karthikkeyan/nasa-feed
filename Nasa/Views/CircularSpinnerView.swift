//
//  CircularSpinnerView.swift
//  Compass
//
//  Created by Anushree on 18/12/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import UIKit

class CircularSpinnerView: UIView {

    enum Style {
        case small
        case medium
        case large
    }

    enum Alignment {
        case top
        case left
        case bottom
        case right
        case center
    }

    let style: Style

    private var shapeLayer = CAShapeLayer()
    private var gradientLayer = CAGradientLayer()
    private var isAnimating = false {
        didSet {
            isAnimating == true ? startAnimating() : stopAnimating()
        }
    }

    private let lineWidth: CGFloat = 3.0
    private let rotationDuration = 1.0
    private let strokeDuration = 0.7
    private let minimumArcLength: CGFloat = 0.1
    private let maximumArcLength = CGFloat(2 * Double.pi - Double.pi / 4)
    private let initialRotationOffset = CGFloat(-(Double.pi) / 2)

    private let transformRotationKeyPath = "transform.rotation.z"
    private let strokeEndKeyPath = "strokeEnd"
    private let strokeStartKeyPath = "strokeStart"
    private let strokeAnimationKey = "stroke"
    private let rotationAnimationKey = "rotation"

    private override init(frame: CGRect) {
        style = .small

        super.init(frame: style.contentFrame)
        setupUI()
    }

    init(with style: Style) {
        self.style = style

        super.init(frame: style.contentFrame)
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        self.style = .small

        super.init(coder: aDecoder)
        setupUI()
    }

    // MARK: - Public Methods

    func startAnimating() {
        stopAnimating()

        gradientLayer.isHidden = false
        shapeLayer.isHidden = false

        shapeLayer.strokeEnd = proportionFromArcLength(radians: minimumArcLength)
        addAnimation(to: gradientLayer, redraw: false, offset: initialRotationOffset)
    }

    func stopAnimating() {
        shapeLayer.isHidden = true

        gradientLayer.isHidden = true
        gradientLayer.removeAllAnimations()
    }

    // MARK: - Notification

    @objc private func applicationDidEnterBackground() {
        stopAnimating()
    }

    @objc private func applicationWillEnterForeground() {
        startAnimating()
    }

    // MARK: - Helper Methods

    private func setupUI() {
        configureShapeLayer()
        configureGradientLayer()
        refreshSpinnerFrame()

        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    private func configureShapeLayer() {
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.strokeStart = 0
        shapeLayer.actions = ["lineWidth": NSNull(), "strokeStart": NSNull(), "strokeEnd": NSNull()] // Specifying actions provides a smooth effect on animation, commenting this line creates a glitch in the animation
        shapeLayer.isHidden = true
        shapeLayer.lineCap = CAShapeLayerLineCap.round
    }

    private func configureGradientLayer() {
        let color = UIColor(red: 0 / 255, green: 204 / 255, blue: 165 / 255, alpha: 1)
        gradientLayer.colors = [color.cgColor, color.cgColor]
        gradientLayer.mask = shapeLayer
        gradientLayer.isHidden = true

        layer.addSublayer(gradientLayer)
    }

    private func refreshSpinnerFrame() {
        let availableSpace = min(layer.frame.size.width, layer.frame.size.height) - 2 * lineWidth
        let xOffset = ceil(frame.size.width - availableSpace) / 2.0
        let yOffset = ceil(frame.size.height - availableSpace) / 2.0

        shapeLayer.frame = CGRect(x: xOffset, y: yOffset, width: availableSpace, height: availableSpace)
        shapeLayer.path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: availableSpace, height: availableSpace)).cgPath

        gradientLayer.frame = bounds
    }

    private func proportionFromArcLength(radians value: CGFloat) -> CGFloat {
        return CGFloat(fmodf(Float(value), Float(2 * Double.pi))) / CGFloat(2 * Double.pi)
    }

    private func addAnimation(to layer: CAGradientLayer, redraw completePath: Bool, offset rotationOffset: CGFloat) {
        guard let shapeLayer = layer.mask as? CAShapeLayer else {
            return
        }
        var strokeAnimation = CABasicAnimation()
        let currentDistanceToStrokeStart = 2 * Double.pi * Double(shapeLayer.strokeStart)

        if completePath {
            CATransaction.begin()

            strokeAnimation = CABasicAnimation(keyPath: strokeStartKeyPath)
            let newStrokeStart = maximumArcLength - minimumArcLength

            shapeLayer.strokeEnd = proportionFromArcLength(radians: maximumArcLength)
            shapeLayer.strokeStart = proportionFromArcLength(radians: CGFloat(newStrokeStart))

            strokeAnimation.fromValue = 0
            strokeAnimation.toValue = proportionFromArcLength(radians: CGFloat(newStrokeStart))

        } else {
            let strokeFromValue = minimumArcLength
            var rotationStartRadians = rotationOffset

            rotationStartRadians += CGFloat(currentDistanceToStrokeStart)

            let rotationAnimation = CABasicAnimation(keyPath: transformRotationKeyPath)
            rotationAnimation.fromValue = Float(rotationStartRadians)
            rotationAnimation.toValue = Double(rotationStartRadians) + 2 * Double.pi
            rotationAnimation.duration = rotationDuration
            rotationAnimation.repeatCount = Float.infinity
            rotationAnimation.fillMode = .forwards

            shapeLayer.removeAnimation(forKey: rotationAnimationKey)
            shapeLayer.add(rotationAnimation, forKey: rotationAnimationKey)

            CATransaction.begin()
            strokeAnimation = CABasicAnimation(keyPath: strokeEndKeyPath)
            strokeAnimation.fromValue = Float(proportionFromArcLength(radians: strokeFromValue))
            strokeAnimation.toValue = Float(proportionFromArcLength(radians: maximumArcLength))

            shapeLayer.strokeStart = 0
            if let value = strokeAnimation.toValue as? CGFloat {
                shapeLayer.strokeEnd = value
            }
        }

        strokeAnimation.delegate = self
        strokeAnimation.fillMode = .forwards

        CATransaction.setAnimationDuration(strokeDuration)
        shapeLayer.removeAnimation(forKey: strokeAnimationKey)
        shapeLayer.add(strokeAnimation, forKey: strokeAnimationKey)
        CATransaction.commit()
    }

}

// MARK: - CAAnimationDelegate

extension CircularSpinnerView: CAAnimationDelegate {

    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag, anim.isKind(of: CABasicAnimation.self) {
            guard let basicAnimation = anim as? CABasicAnimation, let endValue = shapeLayer.presentation()?.value(forKeyPath: transformRotationKeyPath) as? CGFloat else {
                return
            }
            let isStrokeEnd = basicAnimation.keyPath == strokeEndKeyPath
            let rotationOffset = CGFloat(fmodf(Float(endValue), Float(2 * Double.pi)))
            addAnimation(to: gradientLayer, redraw: isStrokeEnd, offset: rotationOffset)
        }
    }
}

// MARK: - CircularSpinner

extension CircularSpinnerView {

    private static let spinnerContentViewTag = 100

    static func show(in parentView: UIView, style: CircularSpinnerView.Style = .medium, alignment: CircularSpinnerView.Alignment = .center, offset: CGPoint = .zero, colors: [UIColor]? = nil) {

        // Removing Circularspinner if already existing
        hide(from: parentView)

        DispatchQueue.main.async {
            let contentView = UIView(frame: parentView.bounds)
            contentView.translatesAutoresizingMaskIntoConstraints = false
            contentView.tag = spinnerContentViewTag
            parentView.addSubview(contentView)

            NSLayoutConstraint.addConstraintsFixingToBounds(to: contentView, with: parentView)

            let spinnerView = CircularSpinnerView(with: style)
            spinnerView.translatesAutoresizingMaskIntoConstraints = false
            if let colors = colors, !colors.isEmpty { spinnerView.gradientLayer.colors = colors.map { $0.cgColor } }
            contentView.addSubview(spinnerView)
            addSpinnerViewConstraints(to: spinnerView, constraintTo: contentView, parentView: parentView, alignment: alignment, offset: offset)

            spinnerView.isAnimating = true
        }
    }

    static func hide(from parentView: UIView) {
        DispatchQueue.main.async {
            parentView.subviews.forEach {
                guard $0.tag == spinnerContentViewTag else { return }

                $0.removeFromSuperview()
            }
        }
    }

    private static func addSpinnerViewConstraints(to spinnerView: CircularSpinnerView, constraintTo contentView: UIView, parentView: UIView, alignment: CircularSpinnerView.Alignment, offset: CGPoint) {
        let xConstraint: NSLayoutConstraint
        let yConstraint: NSLayoutConstraint
        let spinnerSize = spinnerView.style.contentFrame
        switch alignment {
        case .top, .bottom:
            xConstraint = spinnerView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: offset.x)

            if alignment == .top {
                yConstraint = spinnerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: offset.y)
            } else {
                yConstraint = spinnerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: offset.y)
            }

        case .left, .right:
            yConstraint = spinnerView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)

            if alignment == .left {
                xConstraint = spinnerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: offset.x)
            } else {
                xConstraint = spinnerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: offset.x)
            }

        case .center:
            xConstraint = spinnerView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: offset.x)
            yConstraint = spinnerView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: offset.y)
        }

        let height = spinnerView.heightAnchor.constraint(equalToConstant: spinnerSize.height)
        let width = spinnerView.widthAnchor.constraint(equalToConstant: spinnerSize.width)
        NSLayoutConstraint.activate([ xConstraint, yConstraint, width, height ])
    }

}

// MARK: - CircularSpinnerView.SpinnerStyle

private extension CircularSpinnerView.Style {
    var contentFrame: CGRect {
        switch self {
        case .small:
            return CGRect(x: 0, y: 0, width: 24, height: 24)
        case .medium:
            return CGRect(x: 0, y: 0, width: 40, height: 40)
        case .large:
            return CGRect(x: 0, y: 0, width: 64, height: 64)
        }
    }
}

// MARK: - NSLayoutConstraint

private extension NSLayoutConstraint {

    static func addConstraintsFixingToBounds(to view: UIView, with superView: UIView) {
        let leading, trailing, top, bottom: NSLayoutConstraint
        if #available(iOS 11, *) {
            leading = view.safeAreaLayoutGuide.leadingAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.leadingAnchor)
            trailing = view.safeAreaLayoutGuide.trailingAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.trailingAnchor)
            top = view.safeAreaLayoutGuide.topAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.topAnchor)
            bottom = view.safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: superView.safeAreaLayoutGuide.bottomAnchor)
        } else {
            leading = view.leadingAnchor.constraint(equalTo: superView.leadingAnchor)
            trailing = view.trailingAnchor.constraint(equalTo: superView.trailingAnchor)
            top = view.topAnchor.constraint(equalTo: superView.topAnchor)
            bottom = view.bottomAnchor.constraint(equalTo: superView.bottomAnchor)
        }

        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([ leading, trailing, top, bottom ])
    }

}
