//
//  SecurityPolicy.swift
//  NetworkKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/9/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation
import Security

public protocol SecurityPolicy {
    var serverHost: String { get }
    var certificateBundle: Bundle { get }
    var publicCertificateFile: (name: String, type: String) { get }

    func evaluate(challenge: URLAuthenticationChallenge) -> (disposition: URLSession.AuthChallengeDisposition, credential: URLCredential?)
}

public extension SecurityPolicy {

    var certificateBundle: Bundle { return .main }

    func evaluate(challenge: URLAuthenticationChallenge) -> (disposition: URLSession.AuthChallengeDisposition, credential: URLCredential?) {
        let file = publicCertificateFile

        guard validateHost(in: challenge),
            let serverTrust = challenge.protectionSpace.serverTrust, validateTrust(serverTrust, in: challenge),
            let urlToCertificate = certificateBundle.url(forResource: file.name, withExtension: file.type),
            let localCertificateData = try? Data(contentsOf: urlToCertificate),
            let localCertificate = SecCertificateCreateWithData(nil, localCertificateData as CFData),
            let publicKeyForLocalCertificate = publicKey(for: localCertificate) else {
            return (.cancelAuthenticationChallenge, nil)
        }

        var isSuccess = false
        for key in publicKeys(for: serverTrust) {
            isSuccess = (key == publicKeyForLocalCertificate)
            if isSuccess { break }
        }

        return isSuccess ? (.useCredential, URLCredential(trust: serverTrust)) : (.cancelAuthenticationChallenge, nil)
    }

    private func validateHost(in challenge: URLAuthenticationChallenge) -> Bool {
        return challenge.protectionSpace.host == serverHost
    }

    private func validateTrust(_ trust: SecTrust, in challenge: URLAuthenticationChallenge) -> Bool {
        // Set SSL policies for domain name check
        let policies = NSMutableArray()
        policies.add(SecPolicyCreateSSL(true, (challenge.protectionSpace.host as CFString)))
        SecTrustSetPolicies(trust, policies)

        var result: SecTrustResultType = .invalid
        SecTrustEvaluate(trust, &result)
        return (result == .unspecified || result == .proceed)
    }

    private func publicKeys(for trust: SecTrust) -> [SecKey] {
        var publicKeys: [SecKey] = []

        for index in 0..<SecTrustGetCertificateCount(trust) {
            if let certificate = SecTrustGetCertificateAtIndex(trust, index), let publicKey = publicKey(for: certificate) {
                publicKeys.append(publicKey)
            }
        }

        return publicKeys
    }

    private func publicKey(for certificate: SecCertificate) -> SecKey? {
        var publicKey: SecKey?

        let policy = SecPolicyCreateBasicX509()
        var trust: SecTrust?
        let trustCreationStatus = SecTrustCreateWithCertificates(certificate, policy, &trust)

        if let trust = trust, trustCreationStatus == errSecSuccess {
            publicKey = SecTrustCopyPublicKey(trust)
        }

        return publicKey
    }

}
