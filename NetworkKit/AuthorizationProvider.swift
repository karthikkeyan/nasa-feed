//
//  AuthorizationProvider.swift
//  NetworkKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/9/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation

public protocol AuthorizationProvider {
    var authorizationHeaders: [String: String]? { get }
}
