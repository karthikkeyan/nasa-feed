//
//  NetworkLog.swift
//  NetworkKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/9/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation

public struct NetworkLog {

    #if DEBUG
    public static var enableLog = true
    #else
    public static var enableLog = false
    #endif

    public static func debug(_ message: Any) {
        guard enableLog else { return }

        print(message)
    }

}
