//
//  DataTaskOperation.swift
//  NetworkKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/9/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation

open class DataTaskOperation: NetworkOperation {

    private var nsaOperationResource: ResourceConvertible?
    private weak var currentSession: URLSession?
    public weak var responseProxy: ResponseProxy?

    // MARK: - Open Methods

    override open func startOperation() {
        // For the purpose of debuging, we spliting guard statement into two
        guard let resource = operationResource else {
            assertionFailure("Operation resource is nil for \(String(describing: self))")
            self.isFinished = true
            return
        }

        nsaOperationResource = resource

        guard let request = resource.request else {
            assertionFailure("Could't generate URL Request for operation \(resource.path)")
            self.isFinished = true
            return
        }

        logRequest(request, resource: resource)

        let session = URLSession.session(for: qualityOfService, delegate: self)
        networkTask = session.dataTask(with: request, completionHandler: handleDataTaskCompletion)
        networkTask?.resume()

        currentSession = session
    }

    public override func finishOperation() {
        super.finishOperation()

        currentSession?.finishTasksAndInvalidate()
        currentSession = nil
    }

    open func operationDidComplete(with data: Data, statusCode: Int) {
        // Sub class should override this
    }

    open func operationDidComplete(with error: Error, statusCode: Int?) {
        // Sub class should override this
    }

    // MARK: - Private methods

    private func handleDataTaskCompletion(data: Data?, response: URLResponse?, error: Error?) {
        defer {
            // It is responseProxy's responsibility to finish the operation
            if responseProxy == nil {
                finishOperation()
            }
        }

        if let error = error {
            logError(error: error)
            if let proxy = responseProxy {
                proxy.operation(self, didCompleteWith: error, statusCode: (response as? HTTPURLResponse)?.statusCode)
            } else {
                operationDidComplete(with: error, statusCode: (response as? HTTPURLResponse)?.statusCode)
            }
        } else {
            /*
             We are NOT checking isCancelled at the beginning for this methods,
             because a request could be cancelled for other reasons as well.
             
             Example: '-1003 Connection failed, unable to reach host'
             
             The above example is not a server error.
             The operation will be cancelled state because the system/phone couldn't reach our server.

             We want to handle cases like above.
             
             Basically,
                - if there is an error we will handle it.
                - if there is NO error, then check for isCancelled
             */
            guard !isCancelled else { return }

            // For debugging purpose, we we are spliting this guard statement from the above.
            guard let response = response as? HTTPURLResponse else {
                assertionFailure("Invalid Response for \(String(describing: self))")
                return
            }

            let unwrappedData = data ?? Data()
            logResponse(data: unwrappedData, statusCode: response.statusCode)
            if let proxy = responseProxy {
                proxy.operation(self, didCompleteWith: unwrappedData, statusCode: response.statusCode)
            } else {
                operationDidComplete(with: unwrappedData, statusCode: response.statusCode)
            }
        }
    }

}

extension DataTaskOperation: URLSessionDataDelegate {

    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        guard !isCancelled, let taskError = error else { return }

        if let proxy = responseProxy {
            proxy.operation(self, didCompleteWith: taskError, statusCode: nil)
        } else {
            operationDidComplete(with: taskError, statusCode: nil)
            finishOperation()
        }
    }

}
