//
//  ResponseProxy.swift
//  NetworkKit
//
//  Created by Karthikkeyan Bala Sundaram on 8/7/18.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation

public protocol ResponseProxy: class {
    func operation(_ operation: DataTaskOperation, didCompleteWith data: Data, statusCode: Int)
    func operation(_ operation: DataTaskOperation, didCompleteWith error: Error, statusCode: Int?)
}
