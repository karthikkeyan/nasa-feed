//
//  NetworkOperation.swift
//  NetworkKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/9/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation

open class NetworkOperation: Operation {

    // MARK: - Public Properties
    public weak var networkTask: URLSessionTask?
    open override var isAsynchronous: Bool { return true }
    open var operationResource: ResourceConvertible? { return nil }

    private var isOperationFinished: Bool = false
    open override var isFinished: Bool {
        get { return isOperationFinished }
        set {
            willChangeValue(forKey: "isFinished")
            isOperationFinished = newValue
            didChangeValue(forKey: "isFinished")
        }
    }

    private var isOperationExecuting: Bool = false
    open override var isExecuting: Bool {
        get { return isOperationExecuting }
        set {
            willChangeValue(forKey: "isExecuting")
            isOperationExecuting = newValue
            didChangeValue(forKey: "isExecuting")
        }
    }

    // MARK: - Open Methods

    open override func start() {
        guard isCancelled == false else {
            return finishOperation()
        }

        startOperation()
    }

    open func startOperation() {
        // subclass should override
    }

    public func finishOperation() {
        isExecuting = false
        isFinished = true
    }

    open override func cancel() {
        super.cancel()

        finishOperation()
    }

    // MARK: - Public Methods

    final public func logRequest(_ request: URLRequest, resource: ResourceConvertible?) {
        if let url = request.url {
            NetworkLog.debug("URL: \(url)")
        }

        if let httpMethod = request.httpMethod {
            NetworkLog.debug("Method: \(httpMethod)")
        }

        if let headers = request.allHTTPHeaderFields {
            NetworkLog.debug("Headers: \(headers)")
        }

        if let body = resource?.body, let bodyString = String(data: body, encoding: .utf8) {
            NetworkLog.debug("Body:")
            NetworkLog.debug(bodyString)
        }
    }

    final public func logError(error: Error) {
        NetworkLog.debug("Error Response:")
        NetworkLog.debug(error)
        NetworkLog.debug("\n\n")
    }

    final public func logResponse(data: Data, statusCode: Int) {
        NetworkLog.debug("Response \(statusCode):")

        if let responseString = String(data: data, encoding: .utf8) {
            NetworkLog.debug(responseString)
        } else {
            NetworkLog.debug("[ EMPTY ]")
        }

        NetworkLog.debug("\n\n")
    }

    // MARK: - Dealloc

    deinit {
        NetworkLog.debug("\(self) deallocated")
    }

}

// MARK: - URLSessionDelegate

extension NetworkOperation: URLSessionDelegate {

    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard let policy = operationResource?.securityPolicy else {
            completionHandler(.performDefaultHandling, nil)
            return
        }

        let result = policy.evaluate(challenge: challenge)
        completionHandler(result.disposition, result.credential)
    }
}

// MARK: - URLSessionTaskDelegate

extension NetworkOperation: URLSessionTaskDelegate {

    public func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        urlSession(session, didReceive: challenge, completionHandler: completionHandler)
    }

}
