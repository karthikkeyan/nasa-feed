//
//  ResourceConvertible.swift
//  NetworkKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/9/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation

public enum RequestMethod: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
    case put = "PUT"
    case head = "HEAD"
}

public protocol ResourceConvertible {
    var method: RequestMethod { get set }
    var baseURL: String { get set }
    var path: String { get set }
    var testResourcePath: String { get set }
    var headers: [String: String] { get set }
    var authorizationProvider: AuthorizationProvider? { get set }
    var securityPolicy: SecurityPolicy? { get set }
    var queryItems: [URLQueryItem]? { get set }
    var body: Data? { get set }
    var request: URLRequest? { get }
}

public extension ResourceConvertible {

    var request: URLRequest? {
        let urlString = "\(baseURL)\(path)"
        guard var components = URLComponents(string: urlString) else { return nil }
        components.queryItems = queryItems

        guard let url = components.url else { return nil }

        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.httpBody = body

        headers.forEach { request.addValue($0.value, forHTTPHeaderField: $0.key) }
        authorizationProvider?.authorizationHeaders?.forEach { request.addValue($0.value, forHTTPHeaderField: $0.key) }

        return request
    }

}
