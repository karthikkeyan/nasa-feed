//
//  URLSession+Helpers.swift
//  NetworkKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/9/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation

public extension URLSession {

    // MARK: - Public Properties
    static let backgroundSessionIdentifier = "com.networkkit.networking.background"

    // TODO: Come up with a better design to reuse URLSession object.
    // The current implementation creates URLSession object for each tasks, this is not a memory efficient approach.
    static func session(for qos: QualityOfService, backgroundIdentifier: String = URLSession.backgroundSessionIdentifier, delegate: URLSessionDelegate? = nil) -> URLSession {
        var configuration: URLSessionConfiguration
        switch qos {
        case .userInteractive, .userInitiated, .utility, .`default`:
            configuration = URLSessionConfiguration.default
        case .background:
            configuration = URLSessionConfiguration.background(withIdentifier: backgroundIdentifier)
        @unknown default:
            configuration = URLSessionConfiguration.default
        }
        configuration.tlsMinimumSupportedProtocol = .tlsProtocol12

        return URLSession(configuration: configuration, delegate: delegate, delegateQueue: nil)
    }

}
