//
//  DownloadTaskOperation.swift
//  NetworkKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/9/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation

open class DownloadTaskOperation: NetworkOperation {

    public var progressHandler: ((Int) -> Void)?
    public private(set) var url: URL

    private weak var currentSession: URLSession?

    public init(url: URL) {
        self.url = url

        super.init()
    }

    // MARK: - Open Methods

    open override func startOperation() {
        let request = URLRequest(url: url)

        logRequest(request, resource: nil)

        let session = URLSession.session(for: qualityOfService, backgroundIdentifier: url.absoluteString, delegate: self)
        session.configuration.requestCachePolicy = .returnCacheDataElseLoad

        networkTask = session.downloadTask(with: request)
        networkTask?.resume()

        currentSession = session
    }

    public override func finishOperation() {
        super.finishOperation()

        currentSession?.finishTasksAndInvalidate()
        currentSession = nil
    }

    open func operationDidComplete(with url: URL) {
        // Sub class should override this
    }

    open func operationDidComplete(with error: Error) {
        // Sub class should override this
    }

}

// MARK: - URLSessionDownloadDelegate

extension DownloadTaskOperation: URLSessionDownloadDelegate {

    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        defer { finishOperation() }
        guard !isCancelled else { return }

        operationDidComplete(with: location)
    }

    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let progress = Int((Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)) * 100)
        progressHandler?(progress)
    }

    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        guard !isCancelled, let downloadError = error else { return }

        operationDidComplete(with: downloadError)
        finishOperation()
    }

}
