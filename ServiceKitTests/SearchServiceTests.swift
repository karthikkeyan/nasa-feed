//
//  SearchServiceTests.swift
//  ServiceKitTests
//
//  Created by Karthikkeyan Bala Sundaram on 4/6/19.
//

import XCTest
@testable import ServiceKit

class SearchServiceTests: XCTestCase {

    func testSearchQuery() {
        let expect = expectation(description: "Test Search Query")
        var request = SearchRequest()
        request.query = "mars"
        request.mediaType = .image
        request.page = 1
        let operation = SearchOperation(request: request) { result in
            switch result {
            case .success(let value):
                XCTAssertTrue(!value.collection.items.isEmpty, "Test Search Query Failed")
            case .failure:
                XCTFail("Test Search Query Failed")
            }

            expect.fulfill()
        }
        NasaOperationQueue.apiQueue.addOperation(operation)
        wait(for: [expect], timeout: 60)
    }

    func testSearchQueryPageLimit() {
        let expect = expectation(description: "Test Search Query Page Limit")
        var request = SearchRequest()
        request.query = "mars"
        request.mediaType = .image
        request.page = 1001
        let operation = SearchOperation(request: request) { result in
            switch result {
            case .success:
                XCTFail("Test Search Query Page Limit Failed")
            case .failure(let error):
                XCTAssertTrue(error.localizedDescription.contains("Maximum"), "Test Search Query Page Limit Failed")
            }

            expect.fulfill()
        }
        NasaOperationQueue.apiQueue.addOperation(operation)
        wait(for: [expect], timeout: 60)
    }

}
