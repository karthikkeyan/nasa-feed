//
//  FeedViewModel.swift
//  ViewModelKit
//
//  Created by Karthikkeyan Bala Sundaram on 4/7/19.
//

import ServiceKit
import UIKit

public struct FeedItem {
    public let data: NasaMedia
    public let media: Link
}

public struct FeedPage {
    public let items: [FeedItem]
    public let indexPaths: [IndexPath]
}

public class FeedViewModel {

    public private(set) var media: [FeedItem] = []
    public var query: String { return filter.query }
    public private(set) var canloadMore = false

    private lazy var filter: SearchRequest = {
        var request = SearchRequest()
        request.mediaType = .image
        request.page = 0
        return request
    }()
    private var isLoading = false

    public init() { }

    public func loadNexPage(query: String, completion: @escaping (Result<FeedPage?, Error>) -> Void) {
        guard !isLoading else { return }

        let clearPrevious = query != filter.query
        if clearPrevious {
            filter.page = 0
            filter.query = query
        }

        isLoading = true
        filter.page += 1
        let currentItemsCount = media.count
        let operation = SearchOperation(request: filter) { [weak self] result in
            defer { self?.isLoading = false }

            switch result {
            case .success(let response):
                var row = currentItemsCount
                var indexPaths = [IndexPath]()
                var newItems = [FeedItem]()
                for item in response.collection.items {
                    guard let data = item.data.first, let link = item.links.first else {
                        continue
                    }

                    newItems.append(FeedItem(data: data, media: link))
                    indexPaths.append(IndexPath(row: row, section: 0))
                    row += 1
                }

                let pageSize = 100
                self?.canloadMore = newItems.count == pageSize

                if clearPrevious {
                    self?.media = newItems
                } else {
                    self?.media.append(contentsOf: newItems)
                }

                completion(.success(FeedPage(items: newItems, indexPaths: indexPaths)))
            case .failure(let error):
                self?.filter.page -= 1
                completion(.failure(error))
            }
        }
        NasaOperationQueue.apiQueue.addOperation(operation)
    }

    public func clearData() {
        filter.query = ""
        media = []
        canloadMore = false
    }

}
