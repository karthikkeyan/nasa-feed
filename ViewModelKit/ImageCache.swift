//
//  ImageCache.swift
//  ViewModelKit
//
//  Created by Karthikkeyan Bala Sundaram on 4/7/19.
//

import UIKit

public class ImageCache {

    struct ImageData {
        let image: UIImage
        let date: Date
    }

    public static let shared = ImageCache()

    // 20 MB
    public let memorySize: Int = 20 * 1024 * 1024
    // This property, presistInDisk, is debatable
    public var presistInDisk: Bool = false {
        didSet {
            guard oldValue != presistInDisk else { return }

            didChangePresistInDesk()
        }
    }

    private var cache: [AnyHashable: ImageData] = [:]
    private let presistanceQueue = DispatchQueue(label: "com.karthik.nasa.ImageCache.presistanceQueue")

    private init() { }

    // MARK: - Public Methods

    public func add(image: UIImage, identifier: String) {
        if shouldPurge(forNewImage: image) {
            purgeOldData()
        }

        let data = ImageData(image: image, date: Date())
        cache[identifier] = data

        if presistInDisk {
            presist(data: data)
        }
    }

    public func image(forIdentifier id: String) -> UIImage? {
        return cache[id]?.image
    }

    // MARK: - Private Methods

    private func didChangePresistInDesk() {
        if presistInDisk {
            saveImagesInMemoryToDisk()
        } else {
            clearImagesFromDisk()
        }
    }

    private func shouldPurge(forNewImage image: UIImage) -> Bool {
        // TODO: Implementation Pending
        return false
    }

    private func purgeOldData() {
        fatalError("Implementation pending")
    }

    private func presist(data: ImageData) {
        presistanceQueue.async {
            fatalError("Implementation pending")
        }
    }

    private func saveImagesInMemoryToDisk() {
        presistanceQueue.async {
            fatalError("Implementation pending")
        }
    }

    private func clearImagesFromDisk() {
        presistanceQueue.async {
            fatalError("Implementation pending")
        }
    }

}
