//
//  AssetDownloader.swift
//  ViewModelKit
//
//  Created by Karthikkeyan Bala Sundaram on 4/7/19.
//

import ServiceKit

public struct AssetDownloader {

    // This addition public method exisit to perform 'sync' operations.
    // The method 'downloadMedia()' works with `@escaping` 'completion' closure.
    // There is no guarentee that the method would return immediately; It could complete in different thread
    // Which is why, the method `cachedImage` is introduced specifically for 'sync' operation
    // This method performs and retuns the value in the same thread
    public static func cachedImage(forItem item: FeedItem) -> UIImage? {
        return ImageCache.shared.image(forIdentifier: item.data.nasaID)
    }

    // This method guarentee to call the `completion` closure in main thread.
    public static func downloadMedia(of item: FeedItem, completion: @escaping (UIImage?) -> Void) {
        if let image = cachedImage(forItem: item) {
            DispatchQueue.main.async { completion(image) }
            return
        }

        let urlString = item.media.href
        guard let url = URL(string: urlString.replacingOccurrences(of: " ", with: "%20")) else {
            DispatchQueue.main.async { completion(nil) }
            return
        }

        let operation = AssetDownloadOperation(url: url) { result in
            switch result {
            case .success(let url):
                // Not wrapping the entier switch-case allows us to reading the `Data` from the temp file a background separate thread
                guard let data = try? Data(contentsOf: url) else {
                    DispatchQueue.main.async { completion(nil) }
                    return
                }

                DispatchQueue.main.async {
                    if let image = UIImage(data: data) {
                        ImageCache.shared.add(image: image, identifier: item.data.nasaID)
                        completion(image)
                    } else {
                        completion(nil)
                    }
                }
            case .failure:
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }

        if let unwrappedOperation = operation {
            NasaOperationQueue.background.addOperation(unwrappedOperation)
        } else {
            completion(nil)
        }
    }

}
