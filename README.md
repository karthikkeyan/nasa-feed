# Nasa Feed

## About

This is an iOS app that exposes the search functionality of the NASA Image and Video Library API.

**Features:**
- A search bar that allows the user to enter and submit a search query.
- Display search results in some sort of list. Each search result displays at least the image and the title.
- Tapping on a search result displays a details screen including the image, title, description, and date it was created.
- Paginated search result


## Download
The codebase is publicly available in gitlabs. You can clone the code using the following command in terminal

```
git clone https://gitlab.com/karthikkeyan/nasa-feed.git
```

Once the project is cloned or downloaded. Move inside the `nasa-feed` folder. There you will be seeing files like `Nasa.xcworkspace` or `Podfile`. If you see them then you are inside the root directory of the project. Follow the below steps to install the dependency libraries.

## Dependencies

### Cocoapods

Cocoa pods is a dependency management library used to add third-party libraries in the project. If you do not have CocoaPods already installed.

* First install `brew` package manager, if it is already not, on your Mac.
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Then install cocoapods using `brew`
```
brew install cocoapods
```

Once cocoapods library is installed, make sure to update the pods repo
```
pod repo update
```
This will take some time. 

Once your cocopods repo is updated, execute the below command to install all the dependency libraries. 
```
pod install
```

### Swift Lint
We are following coding guidelines and rules provide by swiftlint, since it is commonly accepted for swift language

**Install Swiftlint**
```
brew install swiftlint
```

**Upgrade Swiftlint**
```
brew upgrade swiftlint
```

There needs to be a discussion before adding/removing any rules. All the opt-in or opt-out rules were added in `.swiftlint.yml` file.

> NOTE: we are not using `cocoapods` or any other services to add `swiftlint` in the project. Please make sure `swiftlint` is installed in your system via `brew`.

## Build and Run
The project is created using Xcode 10.2 and requires Swift 5.0. Once the project is cloned, just open the `Nasa.xcworkspace` file and hit `Run` from the Toolbar at the top-left corner.


## Project Architecture
This project is architected based on MVVM. Here's the link to refer more about the architecture,
https://medium.com/@karthikkeyan/ios-app-architecture-3f1d1400862f

The architecture is divided into 4 major layer
1. **Presentation Layer**: This is the place where all the View Controllers, Views and UI related utilities goes.
2. **ViewModel**: A data backing layer for ViewController where all the UI related business logic goes. This is layer helps keep the view controller code lightweight.
3. **Service Layer**: This layer responsible for constructing the request for the API calls and parsing the response into a model.
4. **Network Layer**: This layer is a lightweight layer responsible for making the network call using the request data given by Service Layer.


## Unit Tests
The project is set up to run unit tests on every individual target. There are four different targets added in the project
- Nasa (this runs unit test for all targets since it is the main target)
- ViewModelKit
- ServiceKit
- NetworkKit

> I have just added Unit tests for only ServiceKit given the time constraint.


## Third-Party Libraries

### ReachabilitySwift
This is swift based library we are using in this project to monitor the network connectivity of the device. This library notifies network changes like connection lost, wifi, and cellular. It is a replacement for Apple's Reachability sample, re-written in Swift with closures.

**Github Link:** https://github.com/ashleymills/Reachability.swift
