//
//  APIResponse.swift
//  ServiceKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/15/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation

struct APIResponse<T: Codable>: Codable {
    let returnCode: Int
    let returnException: String?
    let returnMessage: String
    let returnObject: T
}
