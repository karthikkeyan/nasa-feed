//
//  APIError.swift
//  ServiceKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/13/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation

public struct APIError: Error, Codable {

    enum CodingKeys: String, CodingKey {
        case errorCode = "code"
        case errorMessage = "reason"
    }

    // -100001 is a custom error code introduced to handle unknown errors, refer 'NetworkErrors.string' file
    public static let unknownErrorCode = -100001
    public static let undefinedErrorCode = Int.max
    public static var unknown: APIError { return .unknown(withStatusCode: unknownErrorCode) }

    public var httpStatusCode: Int = undefinedErrorCode
    public var errorCode: Int?
    public var errorMessage: String

    // MARK: - Init Methods

    public init(httpStatusCode: Int = undefinedErrorCode, errorCode: Int = undefinedErrorCode, errorMessage: String) {
        self.httpStatusCode = httpStatusCode
        self.errorCode = errorCode
        self.errorMessage = errorMessage
    }

    public init(error: Error) {
        let message = String(networkErrorCode: error._code)
        self.errorCode = error._code
        // Check if NetworkErrors.strings contains message for give error code.
        // If not use error object's localizedDescription
        self.errorMessage = message == "\(error._code)" ? error.localizedDescription : message
        self.httpStatusCode = APIError.undefinedErrorCode
    }

    // MARK: - Static Methods

    public static func unknown(withStatusCode code: Int, file: String = #file, function: String = #function, line: Int = #line, parsedObject: String = "None") -> APIError {
        let errorMessage: String
        switch Environment.current {
        case .development, .testing, .mock:
            errorMessage = "\(String(networkErrorCode: APIError.unknownErrorCode))\nHTTP Code: \(code)\nFile: \(file)\nFunction: \(function)\nLine: \(line)\nParsed Object: \(parsedObject)"
        case .production, .stage:
            errorMessage = String(networkErrorCode: APIError.unknownErrorCode)
        }

        return APIError(httpStatusCode: code, errorMessage: errorMessage)
    }
}

// MARK: - LocalizedError

extension APIError: LocalizedError {
    public var errorDescription: String? { return errorMessage }
}
