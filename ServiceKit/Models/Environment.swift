//
//  Environment.swift
//  ServiceKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/13/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation

public enum Environment: String {
    case development = "com.karthik.nasa.dev"
    case testing = "com.karthik.nasa.qa"
    case stage = "com.karthik.nasa.stage"
    case production = "com.karthik.nasa"
    case mock = "com.karthik.nasa.mock"

    public static var current: Environment = {
        guard let identifier = Bundle.main.bundleIdentifier, let environment = Environment(rawValue: identifier) else {
            return .production
        }

        return environment
    }()

    var host: String {
        // TODO: Change the host based on the environment
        return "images-api.nasa.gov"
    }

    var baseURL: String {
        // TODO: Change the baseURL based on the environment
        switch self {
        case .development, .testing, .stage, .production:
            return "https://\(host)"
        case .mock:
            let frameworkBundle = Bundle(for: NasaDataTaskOperation.self)
            return frameworkBundle.url(forResource: "MockData", withExtension: "bundle")?.absoluteString ?? ""
        }
    }

    var assetBaseURL: String {
        // TODO: Change the assetBaseURL based on the environment
        return "https://images-assets.nasa.gov/image"
    }

}
