//
//  APIResponseDecoder.swift
//  ServiceKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/22/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation
import NetworkKit

final class APIResponseDecoder: JSONDecoder {

    private(set) var dateFormat: APIDateTimeFormat = .longFormat

    private override init() {
        super.init()
    }

    convenience init(dateFormat: APIDateTimeFormat = .longFormat) {
        self.init()

        self.dateFormat = dateFormat

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat.rawValue
        dateDecodingStrategy = .formatted(dateFormatter)
    }

    func decode<T>(_ type: T.Type, from data: Data, statusCode: Int, file: String = #file, function: String = #function, line: Int = #line) -> Result<T, APIError> where T: Decodable {
        let result: Result<T, APIError>
        do {
            if data.isEmpty {
                result = .failure(.unknown(withStatusCode: statusCode, file: file, function: function, line: line, parsedObject: String(describing: type)))
            } else {
                // HTTP status code range 201..299 is consider as success response
                do {
                    let object = try decode(type, from: data)
                    result = .success(object)
                } catch {
                    print(error)

                    var error = try decode(APIError.self, from: data)
                    error.httpStatusCode = statusCode
                    result = .failure(error)
                }
            }
        } catch {
            #if DEBUG
            NetworkLog.debug("Error: \(error)\nFile: \(file)\nFunction: \(function)\nLine: \(line)\nParsed Object: \(String(describing: type))")
            assertionFailure(error.localizedDescription)
            #endif

            result = .failure(.unknown(withStatusCode: statusCode, file: file, function: function, line: line, parsedObject: String(describing: type)))
        }

        #if DEBUG
            // Do nothing if it is DEBUG build for development purpose
        #else
        let unauthorizedErrorCode = 401
        if Token.current != nil && statusCode == unauthorizedErrorCode {
            NotificationCenter.default.post(name: .unauthorizedUserNotification, object: nil, userInfo: [UnauthorizedUserNotificationUserInfoKey: String(describing: type)])
        }
        #endif

        return result
    }

}

// MARK: - APIError

extension APIError {

    static func parse(data: Data, httpStatusCode: Int) -> APIError {
        if var error = try? JSONDecoder().decode(APIError.self, from: data) {
            error.httpStatusCode = httpStatusCode
            return error
        } else {
            return APIError(httpStatusCode: httpStatusCode, errorMessage: String(networkErrorCode: httpStatusCode))
        }
    }

}

// MARK: - Notification

public let UnauthorizedUserNotificationUserInfoKey = "UnauthorizedUserNotificationUserInfoKey"

public extension Notification.Name {
    static let unauthorizedUserNotification = Notification.Name("UnauthorizedUserNotification")
}
