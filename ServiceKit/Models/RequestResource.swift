//
//  RequestResource.swift
//  ServiceKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/13/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import NetworkKit

public struct RequestResource: ResourceConvertible {
    public var path: String
    public var testResourcePath: String
    public var method: RequestMethod = .get
    public var baseURL: String = Environment.current.baseURL
    public var headers: [String: String] = [ "Content-Type": "application/json" ]
    public var authorizationProvider: AuthorizationProvider?
    public var queryItems: [URLQueryItem]?
    public var body: Data?
    public var securityPolicy: SecurityPolicy?

    public init(path: String) {
        self.path = path
        self.testResourcePath = ""
    }
}
