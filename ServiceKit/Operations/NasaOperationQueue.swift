//
//  NasaOperationQueue.swift
//  ServiceKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/13/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import UIKit

public class NasaOperationQueue: OperationQueue {

    public static let apiQueue = NasaOperationQueue(qualityOfService: .userInitiated)
    public static let background = NasaOperationQueue(qualityOfService: .background)

    init(qualityOfService: QualityOfService) {
        super.init()

        self.qualityOfService = qualityOfService
    }

}
