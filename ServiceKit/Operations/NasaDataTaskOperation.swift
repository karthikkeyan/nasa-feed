//
//  NasaDataTaskOperation.swift
//  ServiceKit
//
//  Created by Karthikkeyan Bala Sundaram on 11/25/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import NetworkKit

open class NasaDataTaskOperation: DataTaskOperation {

    open override func startOperation() {
        switch Environment.current {
        case .mock:
            DispatchQueue.global().async { self.startMockOperation() }
        case .development, .testing, .stage, .production:
            super.startOperation()
        }
    }

    // MARK: - Private Methods

    private func startMockOperation() {
        guard let resource = operationResource,
            let dataURL = URL(string: "\(Environment.mock.baseURL)/\(resource.testResourcePath)"),
            let data = try? Data(contentsOf: dataURL) else {
            assertionFailure("Couldn't create mock data")
            return
        }

        logResponse(data: data, statusCode: 200)

        let successHTTPCode = 200
        operationDidComplete(with: data, statusCode: successHTTPCode)
    }

}
