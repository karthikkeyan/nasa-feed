//
//  AssetDownloadOperation.swift
//  ServiceKit
//
//  Created by Karthikkeyan Bala Sundaram on 12/12/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import NetworkKit

public class AssetDownloadOperation: DownloadTaskOperation {

    public private(set) var completion: (Result<URL, Error>) -> Void

    public init?(url: URL, completion: @escaping (Result<URL, Error>) -> Void) {
        self.completion = completion

        super.init(url: url)
    }

    public override func operationDidComplete(with url: URL) {
        completion(.success(url))
    }

    public override func operationDidComplete(with error: Error) {
        completion(.failure(APIError(error: error)))
    }

}
