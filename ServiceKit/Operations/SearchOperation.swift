//
//  SearchOperation.swift
//  ServiceKit
//
//  Created by Karthikkeyan Bala Sundaram on 4/6/19.
//

import NetworkKit

public enum MediaType: String, Codable {
    case image
    case video
}

public struct SearchRequest {
    public var query: String = ""
    public var mediaType: MediaType?
    public var page: Int = 0

    public init() { }
}

public struct Link: Codable {
    public let href: String
    public let render: String?
}

public struct NasaMedia: Codable {
    public let title: String
    public let nasaID: String
    public let description508: String?
    public let keywords: [String]?
    public let description: String?
    public let dateCreated: Date
    public let secondaryCreator: String?
    public let center: String
    public let mediaType: MediaType

    enum CodingKeys: String, CodingKey {
        case title
        case nasaID = "nasa_id"
        case description508 = "description_508"
        case keywords
        case description
        case dateCreated = "date_created"
        case secondaryCreator = "secondary_creator"
        case mediaType = "media_type"
        case center
    }
}

public struct SearchResponse: Codable {
    public struct Collection: Codable {
        public struct Item: Codable {
            public let data: [NasaMedia]
            public let links: [Link]
        }

        public let items: [SearchResponse.Collection.Item]
        public let href: String
    }

    public let collection: SearchResponse.Collection
}

final public class SearchOperation: NasaDataTaskOperation {

    public let request: SearchRequest
    public let completion: (Result<SearchResponse, APIError>) -> Void

    public init(request: SearchRequest, completion: @escaping (Result<SearchResponse, APIError>) -> Void) {
        self.request = request
        self.completion = completion

        super.init()
    }

    public override var operationResource: ResourceConvertible? {
        var resource = RequestResource(path: "/search")
        var queryItems = [ URLQueryItem(name: "q", value: request.query), URLQueryItem(name: "page", value: "\(request.page)") ]

        if let mediaType = request.mediaType {
            queryItems.append(URLQueryItem(name: "media_type", value: mediaType.rawValue))
        }

        resource.queryItems = queryItems

        return resource
    }

    public override func operationDidComplete(with data: Data, statusCode: Int) {
        let result = APIResponseDecoder(dateFormat: .longFormatWithTimeZone).decode(SearchResponse.self, from: data, statusCode: statusCode)
        completion(result)
    }

    public override func operationDidComplete(with error: Error, statusCode: Int?) {
        completion(.failure(APIError(error: error)))
    }

}
