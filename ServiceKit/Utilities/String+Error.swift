//
//  String+Error.swift
//  ServiceKit
//
//  Created by Karthikkeyan Bala Sundaram on 12/29/17.
//  Copyright © 2019 Karthik. All rights reserved.
//

import Foundation

extension String {

    init(networkErrorCode: Int) {
        let bundle = Bundle(identifier: "com.karthik.nasa.ServiceKit") ?? Bundle.main
        self = NSLocalizedString("\(networkErrorCode)", tableName: "NetworkErrors", bundle: bundle, comment: "Network error string")
    }

}
