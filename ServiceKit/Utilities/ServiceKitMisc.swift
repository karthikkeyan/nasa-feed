//
//  ServiceKitMisc.swift
//  ServiceKit
//
//  Created by Karthikkeyan Bala Sundaram on 4/6/19.
//

import Foundation

enum APIDateTimeFormat: String {
    case longFormat = "yyyy-MM-dd'T'HH:mm:ss"
    case longFormatWithTimeZone = "yyyy-MM-dd'T'HH:mm:ssX"
    case longFormatWithMillisecondsTimeZone = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    case standard = "MM/dd/yyyy"
    case standardDashSeparator = "yyyy-MM-dd"
}
